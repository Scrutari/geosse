# GeoSSE, carte des initiatives de l'ESS basé sur le moteur Scrutari

Les données de cette carte sont fournies par une instance du moteur Scrutari chargée de récolter des donnéees géoréférencées liées à l'ESS.

Cet exemple montre l'utilisation de ScrutariJs avec d'autres bibliothèques, en particulier Leaflet

Le site : http://apps.socioeco.org/geosse

