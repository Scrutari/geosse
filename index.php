<?php
/***************************************************************
* GeoSSE - SSE initiatives map based on Scrutari search Engine
* http://www.scrutari.net/dokuwiki/scrutaridata:exportapi
*
* Copyright (c) 2016-2017 Vincent Calame - Exemole
* Licensed under MIT (http://en.wikipedia.org/wiki/MIT_License)
*************************************************************/

require("l10n/locparser.php");

$lang = "fr";
if (isset($_REQUEST['lang'])) {
    switch($_REQUEST['lang']) {
        case 'fr':
            $lang = $_REQUEST['lang'];
    }
}

$GLOBALS['geosse'] = array();
$GLOBALS['geosse']['loc'] = array();
initLoc($lang);

function initLoc($lang) {
    $filePath = 'l10n/'.$lang.'/loc.ini';
    if (file_exists($filePath)) {
        $locArray = parseLocArray(file($filePath));
        $GLOBALS['geosse']['rawLocArray'] = $locArray;
        foreach($locArray as $key => $value) {
            $value = str_replace('"', '\"', $value);
            $value = json_decode('"'.$value.'"');
            $GLOBALS['geosse']['loc'][$key] = $value;
        }
    }
}

function loc($key) {
    if (array_key_exists($key, $GLOBALS['geosse']['loc'])) {
        echo $GLOBALS['geosse']['loc'][$key];
    } else {
        echo $key;
    }
}


?>
<!DOCTYPE html>
<html lang="<?php echo $lang; ?>">
<head>
<title>GeoSSE</title>
<script src="static/jquery/1.11.2/jquery.min.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="static/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="static/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="css/geosse.css" rel="stylesheet" type="text/css" />
<!--<link href="images/icon-16.png" type="image/png" rel="icon"/>-->

<link rel="stylesheet" href="static/leaflet/1.0.3/leaflet.css" />
<script src="static/leaflet/1.0.3/leaflet.js"></script>
<link rel="stylesheet" href="static/leaflet/1.0.3/plugins/MarkerCluster.Default.css" />
<link rel="stylesheet" href="static/leaflet/1.0.3/plugins/MarkerCluster.css" />
<script src="static/leaflet/1.0.3/plugins/leaflet.markercluster.js"></script>

<script src="static/scrutarijs/1.1/scrutarijs.js"></script>

<script src="js/geosse_core.js"></script>
<script src="js/geosse_geo.js"></script>
<script src="js/geosse_html.js"></script>
<script src="js/geosse_nominatim.js"></script>
<script src="js/geosse_scrutari.js"></script>
<!-- <script src="js/l10n/<?php echo $lang; ?>.js"></script>-->

<script>
Geosse.lang = "<?php echo $lang; ?>";
Geosse.scrutariLoc = new Scrutari.Loc();

var startZoom = 2;
var startLat = 0;
var startLon = 0;
var options = {
    maxZoom: 14/*,
    minZoom: 0*/
  };

$(function () {
    Geosse.Geo.initMap("map", startLat, startLon, startZoom, options);
    Geosse.scrutariConfig = new Scrutari.Config("geosse","http://sct1.scrutari.net/sct/geosse/", Geosse.lang, "js-geosse");
    Scrutari.Meta.load(Geosse.scrutariConfig, function (scrutariMeta) {
        Geosse.scrutariMeta = scrutariMeta;
        Geosse.init();
    });
    $("#test").click(function () {
        Geosse.Nominatim.currentEntry = {
            boundingbox:["48.8155755","48.902156","2.224122","2.4697602"]
        };
        Geosse.loadGeoJson();
    });
});
</script>
</head>
<body>
<div id="searchPanel">
    <div>
        <h1>Filtre en cours</h1>
        <p id="noneText">Aucun filtre n'est défini</p>
        <div id="currentNominatimArea" style="display: none">
            <p>Territoire :</p>
            <div id="currentNominatimEntry">
            </div>
        </div>
        <div id="currentQueryArea" style="display: none">
            <p>Termes : <span id="currentQuery"></span></p>
        </div>
        
        <h1>Construction du filtre</h1>
        <h2>Zone géographique</h2>
        
        <p>Zone couvrant un territoire (Pays, région, ville) : 
        <p><input type="text" id="nominatimInput">
        <br><button id="nominatimButton">Rechercher un territoire</button>
        <br><small>Géocodage effectué grâce à <a href="http://nominatim.openstreetmap.org/" target="_blank">Nominatim</a></small></p>
        <div id="nominatimResultArea">
        </div>
        <p></p>
        <h2>Termes de recherche</h2>
        
        <p>Termes présents dans les méta-données (titre, mots-clés, etc.)</p>
        <input type="text" id="scrutariInput">
        <div id="submitArea">
            <button id="scrutariButton">Actualiser le filtre</button>
        </div>
        <!--<button id="test">Test</button>-->
    </div>
</div>
<div id="map-layout">
    <div id="map"></div>
</div>
<div id="listPanel">
    <div>
        <h1>Bases</h1>
        <div id="baseArea">
        </div>
        <div style="margin-top: 40px">
           <p>GeoSSE est un outil expérimental développé par <a href="http://www.socioeco.org">socioeco.org</a>.
           <p>Les données sont gérées par le logiciel <a href="http://www.scrutari.net">Scrutari</a>.
           L'affichage cartographique est possible grâce à la bibliothèque <a href="http://leafletjs.com/">Leaflet</a> sur fond de carte élaboré par <a href="http://www.openstreetmap.fr">Openstreetmap France</a> sur la base des données d'<a href="http://www.openstreetmap.org">OpenStreetMap</a>
           <p>Le code source est disponible sur <a href="https://framagit.org/Scrutari/geosse">Framagit</a></p>
           <p>Toutes les remarques sont les bienvenues. Nous sommes intéressées par toute nouvelle source de données.
           <p>Contact : vincent.calame (@) exemole.fr
        </div>
    </div>
</div>
<script>
<?php
foreach($GLOBALS['geosse']['rawLocArray'] as $key => $value) {
    $value = str_replace('"', '\"', $value);
    echo 'Geosse.scrutariLoc.putLoc("'.$key.'","'.$value.'");'."\n";
}
?>
</script>
</body>
</html>