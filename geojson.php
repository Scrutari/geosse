<?php
/***************************************************************
* GeoSSE - SSE initiatives map based on Scrutari search Engine
* http://www.scrutari.net/dokuwiki/scrutaridata:exportapi
*
* Copyright (c) 2016-2017 Vincent Calame - Exemole
* Licensed under MIT (http://en.wikipedia.org/wiki/MIT_License)
*************************************************************/

/*
* Test de la présence d'un paramètre callback.
* s'il est présent, c'est du Json-P :
* l'object Json doit être encapsulé dans l'appel à la fonction callback
*/
$callback = false;
if (isset($_REQUEST['callback'])) {
    $callback = $_REQUEST['callback'];
}
if ($callback) {
    header('Content-Type: text/javascript; charset=utf-8');
} else {
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
}
$lang = "fr";
if (isset($_REQUEST['lang'])) {
    $lang = $_REQUEST['lang'];
}

$url = "http://sct1.scrutari.net/sct/geosse/json?type=geojson&version=3&fieldvariant=export&insert=-searchmeta,-motclearray";
$url .= "&baselist=".urlencode($_REQUEST["base"]);
$url .= "&lang=".urlencode($lang);

$geoJson = file_get_contents($url);

/*
* Écriture des données GeoJson
*/
if ($callback) {
    echo $callback.'('.$geoJson.')';
} else {
    echo $geoJson;
}
