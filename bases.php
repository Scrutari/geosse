<?php
/***************************************************************
* GeoSSE - SSE initiatives map based on Scrutari search Engine
* http://www.scrutari.net/dokuwiki/scrutaridata:exportapi
*
* Copyright (c) 2017 Vincent Calame - Exemole
* Licensed under MIT (http://en.wikipedia.org/wiki/MIT_License)
*************************************************************/
$lang = "fr";
if (isset($_REQUEST['lang'])) {
    switch($_REQUEST['lang']) {
        case 'fr':
            $lang = $_REQUEST['lang'];
    }
}

function printBases() {
    global $lang;
    $url = "http://sct1.scrutari.net/sct/geosse/json?type=base&version=3&lang=".$lang;
    $json = json_decode(file_get_contents($url), true);
    $baseArray = $json["baseArray"];
    $baseCount = count($baseArray);
    $result = "";
    for($i = 0; $i < $baseCount; $i++) {
        $base = $baseArray[$i];
        $attrMap = false;
        if (array_key_exists("attrMap", $base)) {
            $attrMap = $base["attrMap"];
        }
        
        $result .= '<div class="bases-BasePanel">';
        $result .= "<h2>";
        if (array_key_exists("baseicon", $base)) {
            $result .= '<img src="'.$base["baseicon"].'"> ';
        }
        if (array_key_exists("longtitle", $base["phraseMap"])) {
            $result .= $base["phraseMap"]["longtitle"];
        } else {
            $result .= $base["title"];
        }
        $result .= "</h2>";
        $result .= '<div class="bases-InfoPanel">';
        $result .= "<p>";
        $result .= 'Nombre de points : ';
        $result .= $base["stats"]["fiche"];
        $result .= "</p>";
        if (($attrMap) && (array_key_exists("info:site", $attrMap))) {
            $href = $attrMap["info:site"][0];
            $result .= "<p>"."Site : ".'<a href="'.$href.'">'.$href."</a></p>";
        }
        if (($attrMap) && (array_key_exists("info:process", $attrMap))) {
            $process = $attrMap["info:process"][0];
            $result .= "<p>"."Processus de récupération des données : ".$process."</p>";
        }
        $result .= '</div>';
        $result .= '</div>';
        
    }
    return $result;
}


?>

<!DOCTYPE html>
<html lang="<?php echo $lang; ?>">
<head>
<title>GeoSSE</title>
<script src="static/jquery/1.11.2/jquery.min.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="static/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="static/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="css/geosse.css" rel="stylesheet" type="text/css" />
<style>
.bases-BasePanel {
    margin-bottom: 40px;
}

.bases-InfoPanel {
    margin-left: 20px;
}
</style>
</head>
<body>
<div class="container">
<div class="row">
<h1>Liste des bases de GeoSSE</h1>
<div class="bases-BasePanel">
<p>Les bases sont listées par ordre d'intégration dans le moteur.</p>
<p>Les scripts Python cités peuvent être consultés et récupérés sur <a href="https://framagit.org/Scrutari/export-python">framagit.org</a></p>
</div>
<?php echo printBases(); ?>
</div>
</div>
</body>
</html>