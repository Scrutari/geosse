/***************************************************************
* GeoSSE - SSE initiatives map based on Scrutari search Engine
* http://www.scrutari.net/dokuwiki/scrutaridata:exportapi
*
* Copyright (c) 2016-2017 Vincent Calame - Exemole
* Licensed under MIT (http://en.wikipedia.org/wiki/MIT_License)
*************************************************************/

/* global Scrutari,Geosse */

Geosse.Geo = {};

Geosse.Geo.TILES = {
 
    "fr": {
        url:  "http://tilecache.openstreetmap.fr/osmfr/{z}/{x}/{y}.png",
        attribution: "map data \u00a9 <a href='http://osm.org/copyright'>OpenStreetMap</a> under ODbL "
    },
    "_default": {
        url: "http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
        attribution: "Map data © <a href='http://openstreetmap.org'>OpenStreetMap</a> contributors"
    }
    
};

Geosse.Geo.PLACEICON = L.icon({
    iconUrl: 'images/place-arrow.png',
    iconSize: [13, 14],
    iconAnchor: [6, 16]
});

Geosse.Geo.map = null;
Geosse.Geo.highlightShape = null;
Geosse.Geo.highlightMarker = null;
Geosse.Geo.limitShape = null;

Geosse.Geo.initMap = function (mapId, centerLat, centerLon, zoom, options) {
    var map = L.map(mapId).setView([centerLat, centerLon], zoom);
    var tile;
    if (Geosse.Geo.TILES.hasOwnProperty(Geosse.lang)) {
        tile = Geosse.Geo.TILES[Geosse.lang];
    } else {
        tile = Geosse.Geo.TILES["_default"];
    }
    options.attribution = tile.attribution;
    Geosse.Geo.map = map;
    (new L.TileLayer(tile.url, options)).addTo(map);
};


/**
 * Traitement du fichier au format GeoJson récupéré
 */
Geosse.Geo.convertGeoJson = function (data) {
    var map = Geosse.Geo.map;
    L.geoJson(data, {
        onEachFeature: function (feature, marker) {
            var baseInfo = Geosse.getBaseInfoByCode(feature.properties.codebase);
            var iconHtml = Geosse.getIconHtml(baseInfo.markerBackground, baseInfo.icon);
            marker.setIcon(L.divIcon({
                iconAnchor: new L.Point(16,42),
                popupAnchor:new L.Point(0,-42),
                labelAnchor:new L.Point(12,-20),
                className: "storage-drop-icon",
                html: iconHtml
            }));
            marker.bindTooltip(feature.properties.name, {pane: "popupPane"});
            marker.bindPopup(Geosse.getPopupHtml(feature.properties, data));
            var visible = false;
            baseInfo.markerClusterGroup.addLayer(marker);
        }
    });
};

Geosse.Geo.setLayerVisibility = function (baseInfo, visible) {
    baseInfo.visible = visible;
    if (visible) {
        Geosse.Geo.map.addLayer(baseInfo.markerClusterGroup);
    } else {
        Geosse.Geo.map.removeLayer(baseInfo.markerClusterGroup);
    }
};

Geosse.Geo.highlightRectangle = function (bounds) {
    Geosse.Geo.clearHighlight();
    Geosse.Geo.highlightShape = L.rectangle(bounds, {color: "#ff0000"});
    Geosse.Geo.map.addLayer(Geosse.Geo.highlightShape);
    var northEast = bounds.getNorthEast();
    var northWest = bounds.getNorthWest();
    var topmiddleLatLng = L.latLng(northEast.lat, (northEast.lng + northWest.lng)/2);
    Geosse.Geo.highlightMarker = L.marker(topmiddleLatLng, {icon: Geosse.Geo.PLACEICON});
    Geosse.Geo.map.addLayer(Geosse.Geo.highlightMarker);
};

Geosse.Geo.clearHighlight = function () {
    if (Geosse.Geo.highlightShape) {
        Geosse.Geo.map.removeLayer(Geosse.Geo.highlightShape);
    }
    if (Geosse.Geo.highlightMarker) {
        Geosse.Geo.map.removeLayer(Geosse.Geo.highlightMarker);
    }
};

Geosse.Geo.limitRectangle = function (bounds) {
    Geosse.Geo.clearLimit();
    Geosse.Geo.limitShape = L.rectangle(bounds, {color: "#0000ee", opacity: 0.2, clickable: false, fill: false});
    Geosse.Geo.map.addLayer(Geosse.Geo.limitShape);
};

Geosse.Geo.clearLimit = function () {
    if (Geosse.Geo.limitShape) {
        Geosse.Geo.map.removeLayer(Geosse.Geo.limitShape);
    }
};