/***************************************************************
* GeoSSE - SSE initiatives map based on Scrutari search Engine
* http://www.scrutari.net/dokuwiki/scrutaridata:exportapi
*
* Copyright (c) 2016-2017 Vincent Calame - Exemole
* Licensed under MIT (http://en.wikipedia.org/wiki/MIT_License)
*************************************************************/

/* global Scrutari,Geosse */

Geosse.Scrutari = {};

Geosse.Scrutari.init = function () {
    $("#scrutariButton").click(function () {
        Geosse.loadGeoJson();
    });
    $("#scrutariInput").keyup(function (e) {
        if (e.which == 13) {
            $("#scrutariButton").trigger('click');
        }
    });
};

Geosse.Scrutari.getQuery = function () {
    var text = $("#scrutariInput").val();
    text = $.trim(text);
    if (text.length === 0) {
        return null;
    } else {
        return text;
    }
};