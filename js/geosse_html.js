/***************************************************************
* GeoSSE - SSE initiatives map based on Scrutari search Engine
* http://www.scrutari.net/dokuwiki/scrutaridata:exportapi
*
* Copyright (c) 2016-2017 Vincent Calame - Exemole
* Licensed under MIT (http://en.wikipedia.org/wiki/MIT_License)
*************************************************************/

/* global Scrutari,Geosse */

Geosse.Html = {};

Geosse.Html.baseInfo = function (baseInfo) {
    var baseAPIObject = baseInfo.baseAPIObject;
    var html = "";
    html += "<label>";
    html += "<input type='checkbox' data-role='base-checkbox' data-code-key='" + baseInfo.codeKey + "'> ";
    html += "<span style='background-color:" + baseInfo.clusterBackground + "' class='geosse-ClusterBullet'></span> ";
    if (baseAPIObject.baseicon) {
        html += "<img alt='' src='";
        html +=baseAPIObject.baseicon;
        html += "'> ";
    }
    html += Scrutari.escape(baseAPIObject.title);
    html += " (";
    html += baseInfo.count;
    html += ")";
    html += "</label>";
    html += " <small>[";
    if (baseInfo.site) {
        html += "<a target='_blank' href='";
        html += baseInfo.site;
        html += "'>";
        html += "Site";
        html += "</a>";
        html += " - ";
    }
    html += "<a target='_blank' href='";
    html +=  "geojson.php";
    html += "?base=/" + baseAPIObject.authority + "/" + baseAPIObject.basename;
    html +=  "&lang=" + Geosse.lang;
    html += "'>";
    html += "GeoJSON";
    html += "</a>";
    html += "]</small>";
    return html;
};