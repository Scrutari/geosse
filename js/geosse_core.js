/***************************************************************
* GeoSSE - SSE initiatives map based on Scrutari search Engine
* http://www.scrutari.net/dokuwiki/scrutaridata:exportapi
*
* Copyright (c) 2016-2017 Vincent Calame - Exemole
* Licensed under MIT (http://en.wikipedia.org/wiki/MIT_License)
*************************************************************/

/**
 * @namespace
 */
var Geosse = {};

Geosse.lang = null;
Geosse.scrutariLoc = null;
Geosse.scrutariConfig = null;
Geosse.scrutariMeta = null;
Geosse.baseInfoArray = null;
Geosse.baseInfoMap = null;

Geosse.init = function () {
    Geosse.Nominatim.init();
    Geosse.Scrutari.init();
    Geosse.initBaseInfoArray();
};


Geosse.initBaseInfoArray = function () {
    var baseArray = Geosse.scrutariMeta.getBaseArray();
    var baseInfoArray = new Array();
    Geosse.baseInfoMap = new Object();
    for(var i = 0; i < baseArray.length; i++) {
        var baseAPIObject = baseArray[i];
        var count = Geosse.getFicheCount(baseAPIObject, false);
        if (count > 0) {
            var baseInfo = new Geosse.BaseInfo(baseAPIObject, count);
            baseInfoArray[baseInfoArray.length] = baseInfo;
            Geosse.baseInfoMap[baseInfo.codeKey] = baseInfo;
        }
    }
    baseInfoArray = baseInfoArray.sort(function (baseInfo1, baseInfo2) {
       if (baseInfo1.count > baseInfo2.count) {
           return -1;
       } else if (baseInfo1.count < baseInfo2.count) {
           return 1;
       } else if (baseInfo1.baseAPIObject.codebase > baseInfo2.baseAPIObject.codebase) {
           return -1;
       } else if (baseInfo1.baseAPIObject.codebase < baseInfo2.baseAPIObject.codebase) {
           return -1;
       } else {
           return 0;
       }
    });
    var html = "";
    for(var i = 0; i < baseInfoArray.length; i++) {
        var baseInfo = baseInfoArray[i];
        html += "<div>";
        html += Geosse.Html.baseInfo(baseInfo);
        html += "</div>";
    }
    Geosse.baseInfoArray = baseInfoArray;
    $("#baseArea").html(html);
    var $checkbox = $("input[data-role='base-checkbox']");
    $checkbox.attr("checked", true);
    $checkbox.click(function() {
        var checked = this.checked;
        var baseInfo = Geosse.baseInfoMap[$(this).data("codeKey")];
        Geosse.Geo.setLayerVisibility(baseInfo, checked);
    });
};


Geosse.getFicheCount = function (baseAPIObject, langFilter) {
    if (!langFilter) {
        return baseAPIObject.stats.fiche;
    }
    var langArray = baseAPIObject.stats.langArray;
    if (!langArray) {
        return 0;
    }
    var length = langArray.length;
    for(var i = 0; i < length; i++) {
        var langObj = langArray[i];
        if (langObj.lang == Geosse.lang) {
            return langObj.fiche;
        }
    }
    return 0;
};

Geosse.getBaseInfoByCode = function (code) {
    var key = "code_" + code;
    if (Geosse.baseInfoMap.hasOwnProperty(key)) {
        return Geosse.baseInfoMap[key];
    } else {
        return null;
    }
};

Geosse.clear = function () {
    var length = Geosse.baseInfoArray.length;
    for(var i = 0; i < length; i++) {
        Geosse.baseInfoArray[i].markerClusterGroup.clearLayers();
    }
    Geosse.Geo.clearLimit();
};

Geosse.BaseInfo = function (baseAPIObject, count) {
    var _getAttrVal = function (key, defaultval) {
        if (baseAPIObject.attrMap.hasOwnProperty(key)) {
            return baseAPIObject.attrMap[key][0];
        } else {
            return defaultval;
        }
    };
    var uri = "/" + baseAPIObject.authority + "/" + baseAPIObject.basename;
    this.baseAPIObject = baseAPIObject;
    this.visible = true;
    this.count = count;
    this.codeKey = "code_" + baseAPIObject.codebase; 
    this.icon = baseAPIObject.baseicon;
    var clusterColor =   _getAttrVal("css:cluster-color", "white");
    var clusterBackground = _getAttrVal("css:cluster-background", "black");
    this.markerBackground = _getAttrVal("css:marker-background", "white");
    this.site = _getAttrVal("info:site", null);
    this.clusterBackground =  clusterBackground;
    this.markerClusterGroup = new L.MarkerClusterGroup({
        maxClusterRadius: 20,
        iconCreateFunction: function (cluster) {
            var childCount = cluster.getChildCount();
            return new L.DivIcon({
                html: "<div style='background-color:" + clusterBackground + "'><span style='color:" + clusterColor + "'>" + childCount + "</span></div>",
                className: 'marker-cluster',
                iconSize: new L.Point(40, 40)
                
            });
        }
    });
    Geosse.Geo.map.addLayer(this.markerClusterGroup);
};

/**
 * Appel Ajax pour récupérer les données GeoJson
 */
Geosse.loadGeoJson = function () {
    Geosse.clear();
    var done = false;
    var requestParameters = new Object();
    if (Geosse.Nominatim.currentEntry) {
        Geosse.Geo.limitRectangle(Geosse.Nominatim.currentEntry.leafletBounds);
        var boundingbox = Geosse.Nominatim.currentEntry.boundingbox;
        requestParameters["flt-bbox"] = Geosse.Nominatim.currentEntry.scrutariBbox;
        Geosse.Geo.map.fitBounds(Geosse.Nominatim.currentEntry.leafletBounds);
        $("#currentNominatimEntry").html(Scrutari.escape(Geosse.Nominatim.currentEntry.display_name));
        $("#currentNominatimArea").show();
        done = true;
    } else {
        $("#currentNominatimArea").hide();
    }
    var query = Geosse.Scrutari.getQuery();
    if (query === "*") {
        $("#currentQueryArea").hide();
        done = true;
    } else if (query) {
        requestParameters["q"] = query;
        $("#currentQueryArea").show();
        $("#currentQuery").text(query);
        done = true;
    } else {
        $("#currentQueryArea").hide();
    }
    if (done) {
        $("#noneText").hide();
    } else {
        $("#noneText").show();
        return;
    }
    requestParameters["fieldvariant"] = "geosse";
    var _loadGeoJsonCallback = function (data) {
        if (data.features.length === "") {
            alert("_ Aucun résultat pour le filtre");
        } else {
            Geosse.Geo.convertGeoJson(data);
        }
    };
    Scrutari.Ajax.loadGeoJson(Geosse.scrutariConfig, requestParameters, _loadGeoJsonCallback, Geosse.scrutariErrorCallback);
};

Geosse.getIconHtml = function (color, icon) {
  var html =  "<div class='icon_container' style='background-color:"+ color + "'>"
    + "<img src='" + icon + "' alt='' class='picto-Unique'/>"
    + "</div>" 
    + "<div class='icon_arrow' style='border-top-color:" + color + "'>"
    + "</div>";
    return html;
};

/**
 * Transformation du contenu de l'objet properties d'une feature GeoJson en Html pour popup
 */
Geosse.getPopupHtml = function (properties, data) {
    var text = "<div class='popup-Block'>";
    if (properties.hasOwnProperty("logo")) {
        text += "<img src='" + properties.logo + "' alt=''>";
    }
    if (properties.hasOwnProperty("name")) {
        text += "<p class='popup-Title'>" + Scrutari.escape(properties.name) + "</p>";
    }
    if (properties.hasOwnProperty("description")) {
        text += "<p class='popup-Description'>" +  Scrutari.escape(properties.description) + "</p>";
    }
    if (properties.hasOwnProperty("url")) {
        text += "<p class=''>" + "<a href='" + properties.url + "' target='_blank'>" + properties.url + "</a></p>";
    }
    if (properties.hasOwnProperty("year")) {
        text += "<p class=''>" +  properties.year + "</p>";
    }
    if (properties.hasOwnProperty("website")) {
        var website = properties["website"];
        var websitetext = website;
        var idx = website.indexOf("://");
        if (idx > 0) {
            websitetext = website.substring(idx + 3);
        } else {
            website = "http://" + website;
        }
        text += "<p class=''>"
            + "Site web : "
            + "<a href='" + website + "' target='_blank'>"
            +  websitetext
            + "</a>"
            + "</p>";
    }
    if (properties.hasOwnProperty("mattrMap")) {
        if (properties["mattrMap"].hasOwnProperty("sct:tags")) {
            var attrArray = properties["mattrMap"]["sct:tags"];
            var length = attrArray.length;
            text += "<p class=''>";
            text += '<span class="scrutari-intitule-Motcle">';
            if (length === 1) {
                text += "Mot-clé :";
            } else {
                text += "Mots-clés :";
            }
            text += '</span> ';
            for(var i = 0; i < length; i++) {
                if (i > 0) {
                    text += ", ";
                }
                text += Scrutari.Utils.mark(attrArray[i]);
            }
            text += "</p>";
        }
    }
    if (properties.hasOwnProperty('codemotcleArray')) {
        var length = properties.codemotcleArray.length;
        text += "<p class=''>";
        text += '<span class="scrutari-intitule-Motcle">';
        if (length === 1) {
            text += "Mot-clé :";
        } else {
            text += "Mots-clés :";
        }
        text += '</span> ';
        for(var i = 0; i < length; i++) {
            if (i > 0) {
                text += ", ";
            }
            var code = properties.codemotcleArray[i];
            text += Scrutari.escape(Geosse.getLabel(data, code));
        }
        text += "</p>";
    }
    text += "</div>";
    return text;
};

Geosse.getLabel = function (data, code) {
  var array = data.motcleArray;
  for(var i =0, len = array.length; i< len; i++) {
        var motcle = array[i];
        if (motcle.codemotcle === code) {
            var labelMap = motcle.labelMap;
            if (labelMap.hasOwnProperty("fr")) {
                return labelMap["fr"];
            } else if (labelMap.hasOwnProperty("en")) {
                return labelMap["en"];
            } else {
                for(var key in labelMap) {
                    return labelMap[key];
                }
            }
        }
  }
  return "";
};

Geosse.scrutariErrorCallback = function (error) {
    if (error.parameter !== "q") {
        Scrutari.logError(error);
        return;
    }
    var title =  Geosse.scrutariLoc.loc(error.key);
    if (title !== error.key) {
        var alertMessage = title;
        if (error.hasOwnProperty("array"))  {
            alertMessage += Geosse.scrutariLoc.loc('_ scrutari-colon');
            for(var i = 0; i < error.array.length; i++) {
                alertMessage += "\n";
                var obj = error.array[i];
                alertMessage += "- ";
                alertMessage += Geosse.scrutariLoc.loc(obj.key);
                if (obj.hasOwnProperty("value")) {
                    alertMessage += Geosse.scrutariLoc.loc('_ scrutari-colon');
                    alertMessage += " ";
                    alertMessage += obj.value;
                }
            }
        }
        alert(alertMessage);
    } else {
        Scrutari.logError(error);
    }
};

