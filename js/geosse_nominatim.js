/***************************************************************
* GeoSSE - SSE initiatives map based on Scrutari search Engine
* http://www.scrutari.net/dokuwiki/scrutaridata:exportapi
*
* Copyright (c) 2016 Vincent Calame - Exemole
* Licensed under MIT (http://en.wikipedia.org/wiki/MIT_License)
*************************************************************/

/* global Scrutari,Geosse,L */

Geosse.Nominatim = {};

Geosse.Nominatim.currentEntry = null;

Geosse.Nominatim.init = function () {
    $("#nominatimButton").click(function () {
      var text = $("#nominatimInput").val();
      text = $.trim(text);
      if (text.length > 0) {
          $.ajax({
                url:  "http://nominatim.openstreetmap.org/search",
                dataType: "jsonp",
                jsonp: "json_callback",
                data: {
                    q: text,
                    format: "json",
                    limit: 5,
                    email: "vincent.calame@exemole.fr",
                },
                success: function (data, textStatus) {
                    Geosse.Nominatim.updateEntries(data);
                }
            });
      }
    });
    $("#nominatimInput").keyup(function (e) {
        if (e.which == 13) {
            $("#nominatimButton").trigger('click');
        }
    });
};

Geosse.Nominatim.updateEntries = function (data) {
    var length = data.length;
    if (length == 0) {
        return;
    }
    var html = "";
    for(var i = 0; i < length; i++) {
        var entry = data[i];
        if (!entry.boundingbox) {
            continue;
        }
        html += "<div class='geosse-NominatimEntry' id='place_" + entry.place_id + "'>";
        html += Scrutari.escape(entry.display_name);
        html += "</div>";
    }
    $("#nominatimResultArea").html(html);
    for(var i = 0; i < length; i++) {
        var entry = data[i];
        if (!entry.boundingbox) {
            continue;
        }
        var boundingbox = entry.boundingbox;
        entry.leafletBounds = L.latLngBounds(L.latLng(boundingbox[0], boundingbox[2]), L.latLng(boundingbox[1], boundingbox[3]));
        entry.scrutariBbox = boundingbox[2] + "," + boundingbox[0] + "," + boundingbox[3] + "," + boundingbox[1];
        $entry = $("#place_" + entry.place_id);
        $entry.data("nominatimEntry", entry);
        $entry.click(function () {
            Geosse.Nominatim.currentEntry = $(this).data("nominatimEntry");
            $(".activeNominatimEntry").removeClass("activeNominatimEntry");
            $(this).addClass("activeNominatimEntry");
        });
        $entry.hover(function () {
            Geosse.Geo.highlightRectangle($(this).data("nominatimEntry").leafletBounds);
        }, function () {
            Geosse.Geo.clearHighlight();
        });
    }
}